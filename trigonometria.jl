# MAC0110 - MiniEP7
# <Willian Wang> - <11735380>

function taylor_sin(x)
    k = 15
    xnume = big(0.0)
    xdeno = factorial(big(2k+1))

    for i in 0:k
        val = div(xdeno, factorial(big(2i+1)))
        val *= big(x)^(2i+1)
        if i % 2 == 0
            xnume += val
        else
            xnume -= val
        end
    end

    return xnume / xdeno
end

function taylor_cos(x)
    k = 15
    xnume = big(0.0)
    xdeno = factorial(big(2k))

    for i in 0:k
        val = div(xdeno, factorial(big(2i)))
        val *= big(x)^(2i)
        if i % 2 == 0
            xnume += val
        else
            xnume -= val
        end
    end

    return xnume / xdeno
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function taylor_tan(x)
    k = 15
    nume = Vector{BigInt}(undef, k)
    deno = Vector{BigInt}(undef, k)

    xdeno = big(1)
    for i in 1:k
        b = bernoulli(i)
        nume[i] = numerator(b)
        nume[i] *= big(2)^2i
        nume[i] *= big(2)^2i -1

        deno[i] = denominator(b)
        deno[i] *= factorial(big(2i))
        
        xdeno *= deno[i]
    end
    
    xnume = big(0.0)
    for i in 1:k
        aux = div(xdeno, deno[i])
        aux *= nume[i]
        aux *= big(x)^(2i-1)
        xnume += aux
    end
    
    return xnume / xdeno
end

function check_sin(value, x)
    d = abs(value - taylor_sin(x))
    return d < 0.0001
end

function check_cos(value, x)
    d = abs(value - taylor_cos(x))
    return d < 0.0001
end

function check_tan(value, x)
    d = abs(value - taylor_tan(x))
    return d < 0.0001
end

using Test
function test()
    @test check_sin(0, 0)
    @test check_sin(0.5, pi/6)
    @test check_sin(1, pi/2)
    @test check_sin(0.5, 5pi/6)
    @test check_cos(1, 0)
    @test check_cos(0.5, pi/3)
    @test check_cos(0, pi/2)
    @test check_cos(-0.5, 2pi/3)
    @test check_cos(-1, pi)
    @test check_tan(1, pi/4)
end